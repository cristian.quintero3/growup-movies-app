export const config = {
  apiKey: import.meta.env.VITE_TMDB_API_KEY,
  tmdbURL: import.meta.env.VITE_TMDB_BASE_URL,
  tmdbImgURL: import.meta.env.VITE_TMDB_IMG_URL,
  trendingMovies: '/movie/now_playing',
  movieDetailById: '/movie',
  moviesBySearch: '/search/movie?query'
}
