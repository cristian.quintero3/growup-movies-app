export enum IOriginalLanguage {
 En = 'en',
 Fr = 'fr',
 Ja = 'ja',
 Uk = 'uk',
}

export interface IMovie {
 adult: boolean;
 backdrop_path: string;
 genre_ids: number[];
 id: number;
 original_language: IOriginalLanguage;
 original_title: string;
 overview: string;
 popularity: number;
 poster_path: string;
 release_date: Date;
 title: string;
 video: boolean;
 vote_average: number;
 vote_count: number;
}

export interface IMappedMovie {
  id: number,
  title: string,
  poster: string
}

export interface IDates {
 maximum: Date;
 minimum: Date;
}

export interface ITmdbResponse {
 dates: IDates;
 page: number;
 results: IMovie[];
 total_pages: number;
 total_results: number;
}
