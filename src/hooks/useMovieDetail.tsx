import { IMappedMovieDetail } from 'interfaces/movie-detail'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { getMovieById } from 'services/movies'

export default function useMovieDetail () {
  const [movieData, setMovieData] = useState<IMappedMovieDetail>({
    background: '',
    overview: '',
    poster: '',
    title: ''
  })
  const [isLoading, setIsLoading] = useState(false)
  const { id } = useParams()

  useEffect(() => {
    setIsLoading(true)
    getMovieById({ id: Number(id) })
      .then(movie => {
        document.title = movie.title
        setMovieData(movie)
      })
      .finally(() => setIsLoading(false))
  }, [id])

  return { movieData, isLoading }
}
