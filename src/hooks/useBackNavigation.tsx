import { useNavigate } from 'react-router-dom'

export default function useBackNavigation () {
  const navigate = useNavigate()

  const handleBackNavigation = () => navigate(-1)

  return { handleBackNavigation }
}
