import { config } from 'config'

const { tmdbImgURL } = config

export const getPosterImgURL = ({ path }: { path: string }) => `${tmdbImgURL}/${path}`
