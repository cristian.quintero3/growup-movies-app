import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IMappedMovie } from 'interfaces/movie'
import { getTrendingMovies } from 'services/movies'

const initialMovies: IMappedMovie[] = []

export const getThunkMovies = createAsyncThunk('movies/getMovies', async () => {
  return await getTrendingMovies()
})

export const moviesSlice = createSlice({
  name: 'movies',
  initialState: {
    trendingMovies: initialMovies,
    isLoading: true,
    error: ''
  },
  reducers: {
    setTrendingMovies: (state, action: PayloadAction<IMappedMovie[]>) => ({
      ...state,
      trendingMovies: action.payload
    })
  },
  extraReducers: (builder) => {
    builder.addCase(getThunkMovies.pending, (state) => ({
      ...state,
      isLoading: true
    }))
    builder.addCase(getThunkMovies.rejected, (state, action) => ({
      ...state,
      error: action.error.message || ''
    }))
    builder.addCase(getThunkMovies.fulfilled, (state, action) => ({
      ...state,
      isLoading: false,
      trendingMovies: action.payload
    }))
  }
})

export const { setTrendingMovies } = moviesSlice.actions

export default moviesSlice.reducer
