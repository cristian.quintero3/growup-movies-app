import axios from 'axios'
import { config } from 'config'
import { IMovie, IMappedMovie } from 'interfaces/movie'
import { IMappedMovieDetail, IMovieDetail } from 'interfaces/movie-detail'
import { getPosterImgURL } from 'utils'

const { apiKey, tmdbURL, trendingMovies, movieDetailById } = config

const tmdbApi = axios.create({
  baseURL: tmdbURL,
  headers: {
    Authorization: `Bearer ${apiKey}`
  }
})

const getMoviesData = ({ movies }: { movies: IMovie[] }): IMappedMovie[] => {
  return movies.map(movie => {
    const { id, title, poster_path: posterPath } = movie
    return {
      id,
      title,
      poster: getPosterImgURL({ path: posterPath })
    }
  })
}

export const getTrendingMovies = async () => {
  try {
    const response = await tmdbApi.get(trendingMovies)
    const movies: IMovie[] = response.data.results
    return getMoviesData({ movies })
  } catch (err) {
    console.log(err)
    return []
  }
}

export const getMovieById = async ({ id }: { id: number }): Promise<IMappedMovieDetail> => {
  try {
    const response = await tmdbApi.get(`${movieDetailById}/${id}`)
    const movie: IMovieDetail = response.data
    const { title, overview, poster_path: posterPath, backdrop_path: backdropPath } = movie
    return ({
      title,
      overview,
      poster: getPosterImgURL({ path: posterPath }),
      background: getPosterImgURL({ path: backdropPath })

    })
  } catch (err) {
    console.log(err)
    return {
      title: '',
      overview: '',
      poster: '',
      background: ''
    }
  }
}
