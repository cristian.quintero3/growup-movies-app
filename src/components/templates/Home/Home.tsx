import ListOfMovies from 'components/organisms/ListOfMovies/ListOfMovies'
import { IMappedMovie } from 'interfaces/movie'

export default function Home ({ movies }: { movies: IMappedMovie[] }) {
  return (
    <main>
      <ListOfMovies movies={movies}/>
    </main>
  )
}
