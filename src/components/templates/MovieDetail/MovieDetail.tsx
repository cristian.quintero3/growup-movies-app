import Button from 'components/atoms/Button/Button'
import { BackArrowIcon } from 'components/atoms/Icons/Icons'
import useBackNavigation from 'hooks/useBackNavigation'
import { IMappedMovieDetail } from 'interfaces/movie-detail'
import './MovieDetail.scss'

export default function MovieDetail ({ detail }: { detail: IMappedMovieDetail}) {
  const { handleBackNavigation } = useBackNavigation()
  const { background, overview, poster, title } = detail
  return (
    <main>
      <img className='background' src={background} />
      <Button action={handleBackNavigation} children={<BackArrowIcon />} text={null} />
      <section className='section'>
        <figure>
          <img className='poster' src={poster} alt={`${title}'s movie poster`} />
        </figure>
        <article>
        <h1>{title}</h1>
        <p>{overview}</p>
        </article>
      </section>
    </main>
  )
}
