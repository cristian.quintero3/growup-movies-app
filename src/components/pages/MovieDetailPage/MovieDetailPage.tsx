import MovieDetail from 'components/templates/MovieDetail/MovieDetail'
import useMovieDetail from 'hooks/useMovieDetail'

export default function MovieDetailPage () {
  const { movieData } = useMovieDetail()
  return (
    <MovieDetail detail={movieData} />
  )
}
