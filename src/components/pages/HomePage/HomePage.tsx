import Home from 'components/templates/Home/Home'
import { useAppDispatch, useAppSelector } from 'hooks/store'
import { useEffect } from 'react'
import { getThunkMovies } from 'store/movies/movies.slice'

export default function HomePage () {
  const trendingMovies = useAppSelector(state => state.movies.trendingMovies)
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(getThunkMovies())
  }, [dispatch])
  return (
    <Home movies={trendingMovies} />
  )
}
