import MoviePoster from 'components/atoms/MoviePoster/MoviePoster'
import { Link } from 'react-router-dom'

export default function Movie (movie: { movieId: number, movieTitle: string, posterPath: string }) {
  const { movieId, movieTitle, posterPath } = movie
  return (
    <li className='movie'>
      <Link to={`/movie/${movieId}`}>
        <MoviePoster pathToImg={posterPath} title={movieTitle} />
        <strong>{movieTitle}</strong>
      </Link>
    </li>
  )
}
