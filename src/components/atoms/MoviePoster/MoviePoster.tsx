import './MoviePoster.scss'

export default function MoviePoster ({ pathToImg, title }: { pathToImg: string, title: string }) {
  return (
    <figure>
      <img className='poster' src={pathToImg} alt={`${title}'s movie poster`} />
    </figure>
  )
}
