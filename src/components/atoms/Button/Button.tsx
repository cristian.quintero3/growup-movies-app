import { MouseEventHandler, ReactElement } from 'react'

export default function Button ({ text = '', action, children }: { text: string | null, action: MouseEventHandler, children: ReactElement }) {
  return (
    <button onClick={action}>
      {children}
      {text}
    </button>
  )
}
