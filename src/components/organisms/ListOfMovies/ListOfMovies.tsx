import Movie from 'components/molecules/Movie/Movie'
import { IMappedMovie } from 'interfaces/movie'
import './ListOfMovies.scss'

export default function ListOfMovies ({ movies }: { movies: IMappedMovie[] }) {
  return (
    <section>
      <ul className='list-of-movies'>
        {
          movies.map(movie => {
            const { id, poster, title } = movie
            return (
              <Movie key={id} movieId={id} posterPath={poster} movieTitle={title} />
            )
          })
        }
      </ul>
    </section>
  )
}
