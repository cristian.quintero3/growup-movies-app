import HomePage from 'components/pages/HomePage/HomePage'
import { Suspense, lazy } from 'react'
import { Route, Routes } from 'react-router-dom'

function App () {
  const LazyMovieDetailPage = lazy(() => import('components/pages/MovieDetailPage/MovieDetailPage'))
  return (
    <>
      <Suspense>
        <Routes>
          <Route path='/' element={<HomePage />}/>
          <Route path='/movie/:id' element={<LazyMovieDetailPage />} />
        </Routes>
      </Suspense>
    </>
  )
}

export default App
